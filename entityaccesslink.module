<?php

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Implements hook_entity_access().
 */
function entityaccesslink_entity_access(EntityInterface $entity, $operation, AccountInterface $account) {
  if ($operation === 'view' && $entity instanceof ContentEntityInterface) {
    foreach ($entity as $field) {
      assert($field instanceof FieldItemListInterface);
      if ($field->getFieldDefinition()->getType() === 'entityaccesslink') {
        $authorizingTokens = [];
        foreach ($field as $fieldItem) {
          assert($fieldItem instanceof FieldItemInterface);
          $authorizingTokens[] = $fieldItem->value;
        }
        $providedToken = Drupal::request()->query->get('eal');
        // Note that it's important for access security to return neutral access
        // with query cache context also for an empty query.
        return AccessResult::allowedIf(in_array($providedToken, $authorizingTokens))
          ->addCacheContexts(['url.query_args:eal']);
      }
    }
  }
}

/**
 * Implements hook_field_widget_info_alter().
 */
function entityaccesslink_field_widget_info_alter(array &$info) {
  $info['string_textfield']['field_types'][] = 'entityaccesslink';
}

/**
 * Implements hook_field_formatter_info_alter().
 */
function entityaccesslink_field_formatter_info_alter(array &$info) {
  $info['string']['field_types'][] = 'entityaccesslink';
}