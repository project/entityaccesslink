<?php

namespace Drupal\entityaccesslink\Plugin\Field\FieldType;

use Drupal\Core\Field\Annotation\FieldType;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;

/**
 * Plugin implementation of the 'entityaccesslink' field type.
 *
 * @FieldType(
 *   id = "entityaccesslink",
 *   label = @Translation("Entity Access Link Token(s)"),
 *   description = @Translation("This field stores one or more tokens to create view access links."),
 *   default_widget = "string_textfield",
 *   default_formatter = "string",
 * )
 */
class EntityAccessLinkFieldItem extends StringItem {}
