# Entity Access Link

To add access link for e.g. node type article:
- go to article content type fields
- Add a new multi-valued field of type "Entity Access Link"
- For that field, add as authoring descriptions something like this: 
  "Add secret texts that will allow viewing this article (and for now, avoid special characters). 
  If you e.g. add a secret 'holgers-preview-secret-is-936745267286312836172152945' to /article/foo, 
  then the preview link is /article/foo?eal=holgers-preview-secret-is-936745267286312836172152945"
